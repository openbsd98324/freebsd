# freebsd




![](medias/freebsd-boot.png)

# Intel,. Notebook eeepc 

````
CPU: Intel(R) Celeron(R) CPU  N3050  @ 1.60GHz (1600.05-MHz 686-class CPU)
  Origin="GenuineIntel"  Id=0x406c3  Family=0x6  Model=0x4c  Stepping=3
  Features=0xbfebfbff<FPU,VME,DE,PSE,TSC,MSR,PAE,MCE,CX8,APIC,SEP,MTRR,PGE,MCA,CMOV,PAT,PSE36,CLFLUSH,DTS,ACPI,MMX,FXSR,SSE,SSE2,SS,HTT,TM,PBE>
  Features2=0x43d8e3bf<SSE3,PCLMULQDQ,DTES64,MON,DS_CPL,VMX,EST,TM2,SSSE3,CX16,xTPR,PDCM,SSE4.1,SSE4.2,MOVBE,POPCNT,TSCDLT,AESNI,RDRAND>
  AMD Features=0x28100000<NX,RDTSCP,LM>
  AMD Features2=0x101<LAHF,Prefetch>
  Structured Extended Features=0x2282<TSCADJ,SMEP,ERMS,NFPUSG>
````



1.) Installation 

"https://download.freebsd.org/ftp/releases/i386/i386/ISO-IMAGES/13.0/FreeBSD-13.0-RELEASE-i386-memstick.img.xz" 


FreeBSD freebsd 13.0-RELEASE FreeBSD 13.0-RELEASE #0 releng/13.0-n244733-ea31abc261f: Fri Apr  9 04:04:45 UTC 2021     root@releng1.nyi.freebsd.org:/usr/obj/usr/src/i386.i386/sys/GENERIC  i386



2.) At boot loader, type '3' and type: 
How to boot and pass entropy (boot failure) using MBR disk.

````
vbe set 0x17e
````


3.) modules

Add the i915kms:
````
 sysrc kld_list+=i915kms ; pkg install drm-kmod ; stat /boot/modules/i915kms.ko  
 pkg install -y    drm-next-kmod  
 kldload i915kms  
````


"pkg install graphics/drm-fbsd13-kmod" will install the modules at right place.
````
pkg install graphics/drm-fbsd13-kmod
````

4.) loader

/boot/loader.conf contains: hw.vga.textmode="1"
(only a single line!)


5.) Xorg

````
pkg install Xorg ctwm xinit xterm 
````


Showing TWM (default) on eeepc notebook (intel):

![](medias/eeepc-intel-i915-screenshot.png)



FLDE Desktop running on eeepc (intel):

![](medias/eeepc-desktop-fl-1-freebsd-13.png)




# Ryzen (AMD)

Tested on Ryzen 3. Quite easier to install and configure. It works out of the box.

FreeBSD freebsd 13.0-RELEASE FreeBSD 13.0-RELEASE #0 releng/13.0-n244733-ea31abc261f: Fri Apr  9 04:04:45 UTC 2021     root@releng1.nyi.freebsd.org:/usr/obj/usr/src/i386.i386/sys/GENERIC  i386


`/boot/loader.conf`
Empty is fine.


Showing Desktop on Ryzen 3 (amd):

![](medias/freebsd-13-desktop-glxgears-fast-pc-1644940981.png)

````
aalib-1.4.r5_13                ASCII art library
adwaita-icon-theme-40.1.1      GNOME Symbolic Icons
aom-3.2.0_1                    AV1 reference encoder/decoder
appres-1.0.5                   Program to list application's resources
apr-1.7.0.1.6.1_1              Apache Portability Library
argp-standalone-1.3_4          Standalone version of arguments parsing functions from GLIBC
argyllcms-1.9.2_5              ICC compatible color management system
at-spi2-atk-2.34.2             Assisted Technology Provider module for GTK+
at-spi2-core-2.36.0            Assistive Technology Service Provider Interface
atk-2.36.0                     GNOME accessibility toolkit (ATK)
avahi-app-0.8                  Service discovery on a local network
bash-5.1.12                    GNU Project's Bourne Again SHell
bitmap-1.0.9                   Bitmap editor and converter utilities for X
ca_root_nss-3.71               Root certificate bundle from the Mozilla Project
cairo-1.17.4,3                 Vector graphics library with cross-device output support
colord-1.3.5_1                 Manage color profiles to accurately color input/output devices
ctwm-4.0.3,1                   Extension to twm, with support for multiple virtual screens
cups-2.3.3op2                  Common UNIX Printing System
curl-7.80.0                    Command line tool and library for transferring data with URLs
cyrus-sasl-2.1.27_2            RFC 2222 SASL (Simple Authentication and Security Layer)
dav1d-0.9.2                    Small and fast AV1 decoder
db5-5.3.28_8                   Oracle Berkeley DB, revision 5.3
dbus-1.12.20_5                 Message bus system for inter-application communication
dbus-glib-0.112                GLib bindings for the D-BUS messaging system
dejavu-2.37_1                  Bitstream Vera Fonts clone with a wider range of characters
desktop-file-utils-0.26_1      Couple of command line utilities for working with desktop entries
e2fsprogs-1.46.5               Utilities & library to manipulate ext2/3/4 filesystems
e2fsprogs-libblkid-1.46.5      Blkid library from e2fsprogs package
e2fsprogs-libuuid-1.46.5       UUID library from e2fsprogs package
encodings-1.0.5,1              X.Org Encoding fonts
expat-2.4.4                    XML 1.0 parser written in C
ffmpeg-4.4.1_3,1               Realtime audio/video encoder/converter and streaming server
firefox-97.0_3,2               Web browser based on the browser portion of Mozilla
fltk-1.3.8                     Cross-platform C++ graphical user interface toolkit
font-adobe-100dpi-1.0.3_4      X.Org Adobe 100dpi font
font-adobe-75dpi-1.0.3_4       X.Org Adobe 75dpi font
font-adobe-utopia-100dpi-1.0.4_4 X.Org Adobe Utopia 100dpi font
font-adobe-utopia-75dpi-1.0.4_4 X.Org Adobe Utopia 75dpi font
font-adobe-utopia-type1-1.0.4_4 X.Org Adobe Utopia Type1 font
font-alias-1.0.4               X.Org Font aliases
font-arabic-misc-1.0.3_4       X.Org miscellaneous Arabic fonts
font-bh-100dpi-1.0.3_4         X.Org Bigelow Holmes 100dpi font
font-bh-75dpi-1.0.3_4          X.Org Bigelow Holmes 75dpi font
font-bh-lucidatypewriter-100dpi-1.0.3_4 X.Org Bigelow Holmes Lucida TypeWriter 100dpi font
font-bh-lucidatypewriter-75dpi-1.0.3_4 X.Org Bigelow Holmes Lucida TypeWriter 75dpi font
font-bh-ttf-1.0.3_4            X.Org Bigelow & Holmes TTF font
font-bh-type1-1.0.3_4          X.Org Bigelow Holmes Type1 font
font-bitstream-100dpi-1.0.3_4  X.Org Bitstream Vera 100dpi font
font-bitstream-75dpi-1.0.3_4   X.Org Bitstream Vera 75dpi font
font-bitstream-type1-1.0.3_4   X.Org Bitstream Vera Type1 font
font-cronyx-cyrillic-1.0.3_4   X.Org Cronyx Cyrillic font
font-cursor-misc-1.0.3_4       X.Org miscellaneous Cursor fonts
font-daewoo-misc-1.0.3_4       X.Org miscellaneous Daewoo fonts
font-dec-misc-1.0.3_4          X.Org miscellaneous Dec fonts
font-ibm-type1-1.0.3_4         X.Org IBM Type1 font
font-isas-misc-1.0.3_4         X.Org miscellaneous ISAS fonts
font-jis-misc-1.0.3_4          X.Org miscellaneous JIS fonts
font-micro-misc-1.0.3_4        X.Org miscellaneous Micro fonts
font-misc-cyrillic-1.0.3_4     X.Org miscellaneous Cyrillic font
font-misc-ethiopic-1.0.4       X.Org miscellaneous Ethiopic font
font-misc-meltho-1.0.3_4       X.Org miscellaneous Meltho font
font-misc-misc-1.1.2_4         X.Org miscellaneous Misc fonts
font-mutt-misc-1.0.3_4         X.Org miscellaneous Mutt fonts
font-schumacher-misc-1.1.2_4   X.Org miscellaneous Schumacher fonts
font-screen-cyrillic-1.0.4_4   X.Org Screen Cyrillic font
font-sony-misc-1.0.3_4         X.Org miscellaneous Sony fonts
font-sun-misc-1.0.3_4          X.Org miscellaneous Sun fonts
font-winitzki-cyrillic-1.0.3_4 X.Org Winitzki Cyrillic font
font-xfree86-type1-1.0.4_4     X.Org XFree86 Type1 font
fontconfig-2.13.94_1,1         XML-based font configuration API for X Windows
freeglut-3.2.1                 Open source implementation of GLUT library
freetype2-2.11.1               Free and portable TrueType font rendering engine
fribidi-1.0.11                 Free Implementation of the Unicode Bidirectional Algorithm
gdbm-1.22                      GNU database manager
gdk-pixbuf2-2.40.0             Graphic library for GTK+
gettext-runtime-0.21           GNU gettext runtime libraries and programs
giflib-5.2.1                   Tools and library routines for working with GIF images
glew-2.2.0_3                   OpenGL Extension Wrangler Library
glib-2.70.3,2                  Some useful routines of C programming (current stable version)
gmp-6.2.1                      Free library for arbitrary precision arithmetic
gnome_subr-1.0                 Common startup and shutdown subroutines used by GNOME scripts
gnupg-2.3.3_2                  Complete and free PGP implementation
gnutls-3.6.16                  GNU Transport Layer Security library
gpgme-1.15.1                   Library to make access to GnuPG easier
graphite2-1.3.14               Rendering capabilities for complex non-Roman writing systems
gsettings-desktop-schemas-41.0 Collection of globally shared GSetting schemas
gtk-update-icon-cache-3.24.26_1 Gtk-update-icon-cache utility from the Gtk+ toolkit
gtk3-3.24.31                   Gimp Toolkit for X11 GUI (current stable version)
harfbuzz-3.2.0                 OpenType text shaping engine
hatari-2.2.1                   Atari ST emulator
hicolor-icon-theme-0.17        High-color icon theme shell from the FreeDesktop project
iceauth-1.0.8_2                ICE authority file utility for X
icu-70.1_1,1                   International Components for Unicode (from IBM)
imlib2-1.7.0,2                 The next generation graphics library for Enlightenment
indexinfo-0.3.1                Utility to regenerate the GNU info page index
jbig2dec-0.19                  Decoder implementation of the JBIG2 image compression format
jbigkit-2.1_1                  Lossless compression for bi-level images such as scanned pages, faxes
jpeg-turbo-2.1.1_1             SIMD-accelerated JPEG codec which replaces libjpeg
lame-3.100_3                   Fast MP3 encoder kit
lcms2-2.12                     Accurate, fast, and small-footprint color management engine
libFS-1.0.8                    The FS library
libGLU-9.0.2_1                 OpenGL utility library
libICE-1.0.10,1                Inter Client Exchange library for X11
libSM-1.2.3,1                  Session Management library for X11
libX11-1.7.2,1                 X11 library
libXScrnSaver-1.2.3_2          The XScrnSaver library
libXau-1.0.9                   Authentication Protocol library for X11
libXaw-1.0.14,2                X Athena Widgets library
libXcomposite-0.4.5,1          X Composite extension library
libXcursor-1.2.0               X client-side cursor loading library
libXdamage-1.1.5               X Damage extension library
libXdmcp-1.1.3                 X Display Manager Control Protocol library
libXext-1.3.4,1                X11 Extension library
libXfixes-6.0.0                X Fixes extension library
libXfont-1.5.4_2,2             X font library
libXfont2-2.0.5                X font library
libXft-2.3.4                   Client-sided font API for X applications
libXi-1.8,1                    X Input extension library
libXinerama-1.1.4_2,1          X11 Xinerama library
libXmu-1.1.3,1                 X Miscellaneous Utilities libraries
libXpm-3.5.13                  X Pixmap library
libXrandr-1.5.2                X Resize and Rotate extension library
libXrender-0.9.10_2            X Render extension library
libXres-1.2.1                  X Resource usage library
libXt-1.2.1,1                  X Toolkit library
libXtst-1.2.3_2                X Test extension
libXv-1.0.11_2,1               X Video Extension library
libXvMC-1.0.12                 X Video Extension Motion Compensation library
libXxf86dga-1.1.5              X DGA Extension
libXxf86vm-1.1.4_3             X Vidmode Extension
libass-0.15.2                  Portable ASS/SSA subtitle renderer
libassuan-2.5.5                IPC library used by GnuPG and gpgme
libdaemon-0.14_1               Lightweight C library that eases the writing of UNIX daemons
libdmx-1.1.4_2                 DMX extension library
libdrm-2.4.109,1               Userspace interface to kernel Direct Rendering Module services
libedit-3.1.20210216,1         Command line editor library
libepoll-shim-0.0.20210418     Small epoll implementation using kqueue
libepoxy-1.5.9                 Library to handle OpenGL function pointer management
libevdev-1.9.1.20200928        Linux Event Device library
libevent-2.1.12                API for executing callback functions on events or timeouts
libffi-3.3_1                   Foreign Function Interface
libfontenc-1.1.4               The fontenc Library
libgcrypt-1.9.4                General purpose cryptographic library based on the code from GnuPG
libglvnd-1.4.0                 GL Vendor-Neutral Dispatch library
libgpg-error-1.43              Common error values for all GnuPG components
libgudev-234                   GObject bindings for libudev
libiconv-1.16                  Character set conversion library
libid3tag-0.15.1b_2            ID3 tags library (part of MAD project)
libidn2-2.3.2                  Implementation of IDNA2008 internationalized domain names
libinput-1.19.1_1              Generic input library
libksba-1.6.0                  Library to make X.509 certificates
liblz4-1.9.3,1                 LZ4 compression library, lossless and very fast
libmtdev-1.1.6_1               Multitouch Protocol Translation Library
libnghttp2-1.46.0              HTTP/2.0 C Library
libogg-1.3.5,4                 Ogg bitstream library
libpaper-1.1.28                Library providing routines for paper size management
libpci-3.7.0_1                 PCI configuration space I/O made easy
libpciaccess-0.16              Generic PCI access library
libpthread-stubs-0.4           Weak aliases for pthread functions
librsvg2-rust-2.52.5_1         Library for parsing and rendering SVG vector-graphic files
libssh2-1.10.0,3               Library implementing the SSH2 protocol
libtasn1-4.18.0                ASN.1 structure parser library
libtheora-1.1.1_7              Theora video codec for the Ogg multimedia streaming system
libudev-devd-0.5.0             libudev-compatible interface for devd
libunistring-0.9.10_1          Unicode string library
libunwind-20201110             Generic stack unwinding library
libv4l-1.20.0_2                Video4Linux library
libva-2.13.0_1                 VAAPI wrapper and dummy driver
libvdpau-1.4                   VDPAU wrapper and tracing library
libvorbis-1.3.7_2,3            Audio compression codec library
libvpx-1.11.0                  VP8/VP9 reference encoder/decoder
libwacom-1.5                   Adds tablet support to libinput
libx264-0.163.3060             H.264/MPEG-4 AVC Video Encoding (Library)
libxcb-1.14_1                  The X protocol C-language Binding (XCB) library
libxkbcommon-1.3.1             Keymap handling library for toolkits and window systems
libxkbfile-1.1.0               XKB file library
libxml2-2.9.12                 XML parser library for GNOME
libxshmfence-1.3_1             Shared memory 'SyncFence' synchronization primitive
llvm13-13.0.1                  LLVM and Clang
lua53-5.3.6                    Powerful, efficient, lightweight, embeddable scripting language
mesa-demos-8.4.0_3             OpenGL demos distributed with Mesa
mesa-dri-21.3.4                OpenGL hardware acceleration drivers for DRI2+
mesa-libs-21.3.4               OpenGL libraries that support GLX and EGL clients
mime-support-3.62              MIME Media Types list
mkfontscale-1.2.1              Creates an index of scalable font files for X
mpdecimal-2.5.1                C/C++ arbitrary precision decimal floating point libraries
mupdf-1.18.0_1,1               Lightweight PDF viewer and toolkit
mutt-2.1.5                     Small but powerful text based program for read/writing e-mail
ncftp-3.2.6_2                  FTP client with advanced user interface
netcat-1.10_3                  Simple utility which reads and writes data across network connections
nettle-3.7.3                   Low-level cryptographic library
npth-1.6                       New GNU Portable Threads
nspr-4.33                      Platform-neutral API for system level and libc like functions
nss-3.75                       Libraries to support development of security-enabled applications
openjpeg-2.4.0                 Open-source JPEG 2000 codec
opus-1.3.1                     IETF audio codec
p11-kit-0.24.0                 Library for loading and enumerating of PKCS#11 modules
pango-1.48.11                  Open-source framework for the layout and rendering of i18n text
pciids-20211124                Database of all known IDs used in PCI devices
pcre-8.45                      Perl Compatible Regular Expressions library
perl5-5.32.1_1                 Practical Extraction and Report Language
pinentry-1.1.1                 Collection of simple PIN or passphrase entry dialogs
pinentry-curses-1.1.1          Curses version of the GnuPG password dialog
pixman-0.40.0_1                Low-level pixel manipulation library
pkg-1.17.5                     Package manager
png-1.6.37_1                   Library for manipulating PNG images
polkit-0.120_1                 Framework for controlling access to system-wide components
portaudio-19.6.0_6,1           Portable cross-platform Audio API
py38-evdev-1.4.0               Bindings to the Linux input handling subsystem
py38-pyudev-0.22.0             Pure Python libudev binding
py38-setuptools-57.0.0         Python packages installer
py38-six-1.16.0                Python 2 and 3 compatibility utilities
python38-3.8.12_1              Interpreted object-oriented programming language
readline-8.1.1                 Library for editing command lines as they are typed
screen-4.8.0_3                 Multi-screen window manager
scrot-1.7                      SCReenshOT - command line screen capture utility
sdl-1.2.15_15,2                Cross-platform multimedia development API
serf-1.3.9_6                   Serf HTTP client library
sessreg-1.1.2                  Manage utmp/wtmp entries for non-init X clients
setxkbmap-1.3.2                Set the keyboard using the X Keyboard Extension
shared-mime-info-2.0_2         MIME types database from the freedesktop.org project
smproxy-1.0.6                  Session Manager Proxy
spidermonkey78-78.9.0_5        Standalone JavaScript based from Mozilla 78-esr
sqlite3-3.35.5_4,1             SQL database engine in a C library
subversion-1.14.1              Version control system
tiff-4.3.0                     Tools and library routines for working with TIFF images
tpm-emulator-0.7.4_2           Trusted Platform Module (TPM) emulator
trousers-0.3.14_3              Open-source TCG Software Stack
twm-1.0.11_1                   Tab Window Manager for the X Window System
unzip-6.0_8                    List, test, and extract compressed files from a ZIP archive
urlview-0.9.20131021_1         URL extractor/launcher
utf8proc-2.6.1_1               UTF-8 processing library
vmaf-2.3.0_2                   Perceptual video quality assessment based on multi-method fusion
wayland-1.20.0                 Core Wayland window system code and protocol
webp-1.2.1                     Google WebP image format conversion tool
x11perf-1.6.1                  X11 server performance test program
x265-3.4_2                     H.265/High Efficiency Video Coding (HEVC) format
xauth-1.1                      X authority file utility
xbacklight-1.2.3               Program to adjust backlight brightness
xbitmaps-1.1.2                 X.Org bitmaps data
xcalc-1.1.0                    Scientific calculator for X
xcb-util-0.4.0_2,1             Module with libxcb/libX11 extension/replacement libraries
xcb-util-wm-0.4.1_3            Framework for window manager implementation
xclock-1.0.9                   Analog and digital clock for X
xcmsdb-1.0.5                   Device Color Characterization utility for X
xconsole-1.0.7_1               Monitor system console messages with X
xcursor-themes-1.0.6           X.org cursors themes
xcursorgen-1.0.7               Create an X cursor file from a collection of PNG images
xdpyinfo-1.3.2_3               Display information utility for X
xdriinfo-1.0.6_4               Query configuration information of DRI drivers
xev-1.2.4                      Print contents of X events
xf86-input-keyboard-1.9.0_4    X.Org keyboard input driver
xf86-input-libinput-0.30.0_1   X.Org libinput input driver
xf86-input-mouse-1.9.3_3       X.Org mouse input driver
xf86-video-scfb-0.0.7          X.Org syscons display driver
xf86-video-vesa-2.5.0          X.Org vesa display driver
xf86dga-1.0.3_1                Test program for the XFree86-DGA extension
xgamma-1.0.6                   Gamma correction through the X server
xgc-1.0.5                      X graphics demo
xhost-1.0.8                    Server access control program for X
xinit-1.4.1,1                  X Window System initializer
xinput-1.6.3                   Very useful utility for configuring and testing XInput devices
xkbcomp-1.4.5                  Compile XKB keyboard description
xkbevd-1.1.4                   XKB event daemon
xkbutils-1.0.4_2               XKB utility demos
xkeyboard-config-2.34          X Keyboard Configuration Database
xkill-1.0.5                    Utility for killing a client by its X resource
xlsatoms-1.1.3                 List interned atoms defined on a server
xlsclients-1.1.4               List client applications running on a display
xmessage-1.0.5                 Display message or query in a X window
xmodmap-1.0.10                 Utility for modifying keymaps and pointer button mappings in X
xorg-7.7_3                     X.Org complete distribution metaport
xorg-apps-7.7_4                X.org apps meta-port
xorg-docs-1.7.1,1              X.org documentation files
xorg-drivers-7.7_6             X.org drivers meta-port
xorg-fonts-7.7_1               X.org fonts meta-port
xorg-fonts-100dpi-7.7          X.Org 100dpi bitmap fonts
xorg-fonts-75dpi-7.7           X.Org 75dpi bitmap fonts
xorg-fonts-cyrillic-7.7        X.Org Cyrillic bitmap fonts
xorg-fonts-miscbitmaps-7.7     X.Org miscellaneous bitmap fonts
xorg-fonts-truetype-7.7_1      X.Org TrueType fonts
xorg-fonts-type1-7.7           X.Org Type1 fonts
xorg-libraries-7.7_4           X.org libraries meta-port
xorg-server-1.20.13,1          X.Org X server and related programs
xorgproto-2021.5               X Window System unified protocol definitions
xpr-1.0.5                      Utility for printing an X window dump
xprop-1.2.5                    Property displayer for X
xrandr-1.5.1                   Primitive command line interface to the RandR extension
xrdb-1.2.0                     X server resource database utility
xrefresh-1.0.6                 Refresh all or part of an X screen
xset-1.2.4_3                   User preference utility for X
xsetroot-1.1.2                 Root window parameter setting utility for X
xterm-370                      Terminal emulator for the X Window System
xtrans-1.4.0                   Abstract network code for X
xvid-1.3.7,1                   Opensource MPEG-4 codec, based on OpenDivx
xvinfo-1.1.4                   Print out X-Video extension adaptor information
xwd-1.0.7                      Dump an image of an X window
xwininfo-1.1.5                 Window information utility for X
xwud-1.0.5                     Image displayer for X
zstd-1.5.0                     Fast real-time compression algorithm
````

